﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "chaiscript/chaiscript.hpp"

static std::string helloWorld(const std::string &t_name) {
  return "Hello© " + t_name + "!";
}

QString getMessage()
{
    chaiscript::ChaiScript chai;
    // class Test
    chai.add(chaiscript::fun(&helloWorld), "helloWorld");

    chaiscript::Boxed_Value bv = chai.eval(R"(
      helloWorld("Bob");
      //123;
    )");
    try {
        return QString::fromStdString(chai.boxed_cast<std::string>(bv));
    } catch(chaiscript::exception::bad_boxed_cast &) {
        return "chaiscript::exception::bad_boxed_cast";
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //this->setWindowTitle(u8"ハロー©");
    this->setWindowTitle(getMessage());
}

MainWindow::~MainWindow()
{
    delete ui;
}

