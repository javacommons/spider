QT += core gui
#QT += script

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TARGET = spider

DESTDIR = $$PWD

include($$PWD/../common/include/include.pri)

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

win32 {
} else {
    TARGET=$$join(TARGET,,,.exe)
}

DISTFILES += \
    ../snap/snapcraft.yaml

# Deployment
isEmpty(PREFIX){
 PREFIX = /usr
}

linux {
    QMAKE_LFLAGS += -Wl,--copy-dt-needed-entries
}

BINDIR  = $$PREFIX/bin
DATADIR = $$PREFIX/share

target.path = $$BINDIR

icon.files = icons/olivia.png
icon.path = $$DATADIR/icons/hicolor/512x512/apps/

desktop.files = spider.desktop
desktop.path = $$DATADIR/applications/

INSTALLS += target icon desktop
